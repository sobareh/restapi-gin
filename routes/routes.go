package routes

import (
	"api-gin/controller"
	"api-gin/middlewares"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.POST("/register", controller.Register)
	r.POST("/login", controller.Login)

	authMiddlewareRoute := r.Group("/age-rating-categories")
	authMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	authMiddlewareRoute.GET("/", controller.GetAllRatings)
	authMiddlewareRoute.POST("/", controller.CreateRating)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
