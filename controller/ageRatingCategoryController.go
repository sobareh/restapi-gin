package controller

import (
	"api-gin/models"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

type ageRatingCategoryInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// GetAllAgeRatingCategory godoc
// @Summary Get all AgeRatingCategory.
// @Description Get a list of AgeRatingCategory.
// @Tags AgeRatingCategory
// @Produce json
// @Success 200 {object} []models.AgeRatingCategory
// @Router /age-rating-categories [get]
func GetAllRatings(c *gin.Context) {
	//	get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.AgeRatingCategory
	db.Find(&ratings)

	c.JSONP(http.StatusOK, gin.H{"status": "success", "data": ratings})
}

// CreateAgeRatingCategory godoc
// @Summary Create new AgeRatingCategory.
// @Description Creating a new AgeRatingCategory.
// @Tags AgeRatingCategory
// @Param Body body ageRatingCategoryInput true "the body to create a new AgeRatingCategory"
// @produce json
// @Success 200 {object} models.AgeRatingCategory
// @Router /age-rating-categories [post]
func CreateRating(c *gin.Context) {
	// Validate input
	var input ageRatingCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//create rating
	rating := models.AgeRatingCategory{Name: input.Name, Description: input.Description}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&rating)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}
